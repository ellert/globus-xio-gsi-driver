globus-xio-gsi-driver (5.4-3) unstable; urgency=medium

  * Enable bind-now hardening
  * Update lintian overrides due to renamed tag

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 06 Mar 2024 18:31:07 +0100

globus-xio-gsi-driver (5.4-2) unstable; urgency=medium

  * Make doxygen Build-Depends-Indep
  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 22:43:48 +0200

globus-xio-gsi-driver (5.4-1) unstable; urgency=medium

  * Typo fixes

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 27 Aug 2021 09:54:35 +0200

globus-xio-gsi-driver (5.3-1) unstable; urgency=medium

  * Minor fixes to makefiles
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop old symlink-to-dir conversion from 2014
  * Drop ancient Replaces/Conflicts/Breaks

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 15 Dec 2020 21:34:40 +0100

globus-xio-gsi-driver (5.2-1) unstable; urgency=medium

  * Update documentation links to always point to the latest documentation

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 03 Sep 2019 04:42:01 +0200

globus-xio-gsi-driver (5.1-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:51 +0200

globus-xio-gsi-driver (5.1-1) unstable; urgency=medium

  * Doxygen fixes
  * Use .maintscript file for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Feb 2019 20:01:02 +0100

globus-xio-gsi-driver (5.0-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:32 +0200

globus-xio-gsi-driver (4.1-1) unstable; urgency=medium

  * GT6 update: Add SNI and ALPN support via cntls

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 10 Sep 2017 09:32:52 +0200

globus-xio-gsi-driver (3.11-2) unstable; urgency=medium

  * Migrate to dbgsym packages

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:38 +0200

globus-xio-gsi-driver (3.11-1) unstable; urgency=medium

  * GT6 update: Fix crash when checking for anonymous GSS name when name
    comparison fails
  * Drop dummy package libglobus-xio-gsi-driver0

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 27 Jun 2017 10:24:50 +0200

globus-xio-gsi-driver (3.10-2) unstable; urgency=medium

  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 02 Sep 2016 16:14:20 +0200

globus-xio-gsi-driver (3.10-1) unstable; urgency=medium

  * GT6 update: Fix anonymous auth in strict mode
  * Drop patch globus-xio-gsi-driver-anon.patch (accepted upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 11 May 2016 16:41:24 +0200

globus-xio-gsi-driver (3.9-2) unstable; urgency=medium

  * Fix broken anonymous authentication (Closes: #823740)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 11 May 2016 14:31:03 +0200

globus-xio-gsi-driver (3.9-1) unstable; urgency=medium

  * GT6 update: Propagate error back to caller when name mismatch occurs
    on server instead of just closing the handle
  * Replace ldconfig postinst/postrm scripts with triggers

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 03 May 2016 15:56:56 +0200

globus-xio-gsi-driver (3.8-2) unstable; urgency=medium

  * Set SOURCE_DATE_EPOCH and rebuild using doxygen 1.8.11
    (for reproducible build)
  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 17:26:05 +0100

globus-xio-gsi-driver (3.8-1) unstable; urgency=medium

  * GT6 update
  * GT-615: GSI XIO driver uses resolved IP address when importing names

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 29 Jul 2015 17:24:02 +0200

globus-xio-gsi-driver (3.7-1) unstable; urgency=medium

  * GT6 update (handle anonymous targets in GSI RFC2818 mode)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 29 May 2015 20:50:17 +0200

globus-xio-gsi-driver (3.6-4) unstable; urgency=medium

  * Rebuild using doxygen 1.8.9.1 (Closes: #630071)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 30 Apr 2015 16:54:06 +0200

globus-xio-gsi-driver (3.6-3) unstable; urgency=medium

  * Add Pre-Depends for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 08 Nov 2014 23:44:48 +0100

globus-xio-gsi-driver (3.6-2) unstable; urgency=medium

  * Properly handle symlink-to-dir conversion in doc package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Nov 2014 17:54:41 +0100

globus-xio-gsi-driver (3.6-1) unstable; urgency=medium

  * GT6 update
  * Drop patch globus-xio-gsi-driver-doxygen.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 27 Oct 2014 14:42:27 +0100

globus-xio-gsi-driver (3.5-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata
  * Rename binary package due to dropped soname (this is a plugin)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 23 Sep 2014 16:33:16 +0200

globus-xio-gsi-driver (2.4-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.5
  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 10 Nov 2013 19:44:36 +0100

globus-xio-gsi-driver (2.3-2) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 17:28:48 +0200

globus-xio-gsi-driver (2.3-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.1
  * Drop patch globus-xio-gsi-driver-doxygen.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 29 Apr 2012 09:39:39 +0200

globus-xio-gsi-driver (2.1-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 18:42:09 +0100

globus-xio-gsi-driver (2.1-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.0
  * Drop patches globus-xio-gsi-driver-deps.patch,
    globus-xio-gsi-driver-funcgrp.patch,
    globus-xio-gsi-driver-type-punned-pointer.patch and
    globus-xio-gsi-driver-wrong-dep.patch (fixed upstream)
  * Make doc package architecture independent

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 28 Dec 2011 14:17:49 +0100

globus-xio-gsi-driver (0.6-9) unstable; urgency=low

  * Use system jquery script

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 07 Jun 2011 03:06:22 +0200

globus-xio-gsi-driver (0.6-8) unstable; urgency=low

  * Add README file
  * Use new doxygen-latex build dependency (Closes: #616250)
  * Add missing dependencies
  * Clear dependency libs in .la file (Closes: #621227)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Apr 2011 11:42:13 +0200

globus-xio-gsi-driver (0.6-7) unstable; urgency=low

  * Converting to package format 3.0 (quilt)
  * Add new build dependency on texlive-font-utils due to changes in texlive
    packaging (epstopdf moved there) (Closes: #583039)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:30 +0200

globus-xio-gsi-driver (0.6-6) unstable; urgency=low

  * Update to Globus Toolkit 5.0.0
  * Add debug package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Jan 2010 23:08:29 +0100

globus-xio-gsi-driver (0.6-5) unstable; urgency=low

  * Fix rule dependencies in the debian/rules file.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 13 May 2009 14:58:35 +0200

globus-xio-gsi-driver (0.6-4) unstable; urgency=low

  * Initial release (Closes: #514477).
  * Fix changed dependency namespace (globus-gssapi-error).

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 29 Apr 2009 07:51:09 +0200

globus-xio-gsi-driver (0.6-3) UNRELEASED; urgency=low

  * Rebuilt to correct libltdl dependency.
  * Preparing for other 64bit platforms than amd64.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 18 Apr 2009 20:17:37 +0200

globus-xio-gsi-driver (0.6-2) UNRELEASED; urgency=low

  * Only quote the Apache-2.0 license if necessary.
  * Updated deprecated Source-Version in debian/control.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 26 Mar 2009 09:21:25 +0100

globus-xio-gsi-driver (0.6-1) UNRELEASED; urgency=low

  * First build.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 03 Jan 2009 11:04:30 +0100
